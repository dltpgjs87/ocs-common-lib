package com.ogqcorp.ocs.utils;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by OGQ Corp. User: iseheon Created At: 2020/02/24 1:58 오후 Last Modified At: 2020/02/24
 */
@Ignore
public class UserAgentUtilTest {
  private static final Logger log = LoggerFactory.getLogger(UserAgentUtilTest.class);
  @Test
  public void uaTest() {
    String ua = "Mozilla/5.0 (iPhone; CPU iPhone OS 5_1_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9B206 Safari/7534.48.3";
    //String ua = "";

    boolean bMobile = UserAgentUtil.isMobile(ua);
    assertThat(bMobile).isTrue();

    String device = UserAgentUtil.deviceInfo(ua);
    assertThat(device).isEqualTo("iPhone");

    String os = UserAgentUtil.osInfo(ua);
    assertThat(os).isEqualTo("iOS.5.1.1");

    String browser = UserAgentUtil.browserInfo(ua);
    assertThat(browser).isEqualTo("Mobile Safari.5.1");
  }

  @Test
  public void uaListTest() {
    Map<String, String> map = new HashMap<>();

    //Android Phone
    map.put("Samsung SM-G960F | Android.8.0.0 | Chrome Mobile.62.0.3202", "Mozilla/5.0 (Linux; Android 8.0.0; SM-G960F Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.84 Mobile Safari/537.36");
    map.put("Samsung SM-G892A | Android.7.0 | Chrome Mobile WebView.60.0.3112", "Mozilla/5.0 (Linux; Android 7.0; SM-G892A Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/60.0.3112.107 Mobile Safari/537.36");
    map.put("Samsung SM-G930VC | Android.7.0 | Chrome Mobile WebView.58.0.3029", "Mozilla/5.0 (Linux; Android 7.0; SM-G930VC Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/58.0.3029.83 Mobile Safari/537.36");
    map.put("Samsung SM-G935S | Android.6.0.1 | Chrome Mobile WebView.55.0.2883", "Mozilla/5.0 (Linux; Android 6.0.1; SM-G935S Build/MMB29K; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/55.0.2883.91 Mobile Safari/537.36");
    map.put("Samsung SM-G920V | Android.6.0.1 | Chrome Mobile.52.0.2743", "Mozilla/5.0 (Linux; Android 6.0.1; SM-G920V Build/MMB29K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.98 Mobile Safari/537.36");
    map.put("Samsung SM-G928X | Android.5.1.1 | Chrome Mobile.47.0.2526", "Mozilla/5.0 (Linux; Android 5.1.1; SM-G928X Build/LMY47X) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.83 Mobile Safari/537.36");
    map.put("Huawei Nexus 6P | Android.6.0.1 | Chrome Mobile.47.0.2526", "Mozilla/5.0 (Linux; Android 6.0.1; Nexus 6P Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.83 Mobile Safari/537.36");
    map.put("G8231 | Android.7.1.1 | Chrome Mobile WebView.59.0.3071", "Mozilla/5.0 (Linux; Android 7.1.1; G8231 Build/41.2.A.0.219; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/59.0.3071.125 Mobile Safari/537.36");
    map.put("G8231 | Android.7.1.1 | Chrome Mobile WebView.59.0.3071", "Mozilla/5.0 (Linux; Android 7.1.1; G8231 Build/41.2.A.0.219; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/59.0.3071.125 Mobile Safari/537.36");
    map.put("HTC One X10 | Android.6.0 | Chrome Mobile WebView.61.0.3163", "Mozilla/5.0 (Linux; Android 6.0; HTC One X10 Build/MRA58K; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/61.0.3163.98 Mobile Safari/537.36");
    map.put("HTC One M9 | Android.6.0 | Chrome Mobile.52.0.2743", "Mozilla/5.0 (Linux; Android 6.0; HTC One M9 Build/MRA58K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.98 Mobile Safari/537.3");

    //Apple iPhone
    map.put("iPhone | iOS.12.0 | Mobile Safari.12.0", "Mozilla/5.0 (iPhone; CPU iPhone OS 12_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15E148 Safari/604.1");
    map.put("iPhone | iOS.12.0 | Chrome Mobile iOS.69.0.3497", "Mozilla/5.0 (iPhone; CPU iPhone OS 12_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/69.0.3497.105 Mobile/15E148 Safari/605.1");
    map.put("iPhone | iOS.12.0 | Firefox iOS.13.2", "Mozilla/5.0 (iPhone; CPU iPhone OS 12_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) FxiOS/13.2b11866 Mobile/16A366 Safari/605.1.15");
    map.put("iPhone | iOS.11.0 | Mobile Safari.11.0", "Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1");
    map.put("iPhone | iOS.11.0 | Mobile Safari.11.0", "Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) Version/11.0 Mobile/15A5341f Safari/604.1");
    map.put("iPhone | iOS.11.0 | Mobile Safari.11.0", "Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A5370a Safari/604.1");
    map.put("iPhone | iOS.10.0.1 | Mobile Safari.10.0", "Mozilla/5.0 (iPhone9,3; U; CPU iPhone OS 10_0_1 like Mac OS X) AppleWebKit/602.1.50 (KHTML, like Gecko) Version/10.0 Mobile/14A403 Safari/602.1");
    map.put("iPhone | iOS.10.0.1 | Mobile Safari.10.0", "Mozilla/5.0 (iPhone9,4; U; CPU iPhone OS 10_0_1 like Mac OS X) AppleWebKit/602.1.50 (KHTML, like Gecko) Version/10.0 Mobile/14A403 Safari/602.1");
    map.put("iPhone | iOS.3.0 | Mobile Safari.3.0", "Mozilla/5.0 (Apple-iPhone7C2/1202.466; U; CPU like Mac OS X; en) AppleWebKit/420+ (KHTML, like Gecko) Version/3.0 Mobile/1A543 Safari/419.3");

    //MS Windows Phone
    map.put("Generic Smartphone | Windows Phone.10.0 | Edge Mobile.15.15254", "Mozilla/5.0 (Windows Phone 10.0; Android 6.0.1; Microsoft; RM-1152) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Mobile Safari/537.36 Edge/15.15254");
    map.put("Generic Smartphone | Windows Phone.10.0 | Edge Mobile.12.10536", "Mozilla/5.0 (Windows Phone 10.0; Android 4.2.1; Microsoft; RM-1127_16056) AppleWebKit/537.36(KHTML, like Gecko) Chrome/42.0.2311.135 Mobile Safari/537.36 Edge/12.10536");
    map.put("Lumia 950 | Windows Phone.10.0 | Edge Mobile.13.1058", "Mozilla/5.0 (Windows Phone 10.0; Android 4.2.1; Microsoft; Lumia 950) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Mobile Safari/537.36 Edge/13.1058");

    //Tablet
    map.put("Pixel C | Android.7.0 | Chrome Mobile WebView.52.0.2743", "Mozilla/5.0 (Linux; Android 7.0; Pixel C Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/52.0.2743.98 Safari/537.36");
    map.put("SGP771 | Android.6.0.1 | Chrome Mobile WebView.52.0.2743", "Mozilla/5.0 (Linux; Android 6.0.1; SGP771 Build/32.2.A.0.253; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/52.0.2743.98 Safari/537.36");
    map.put("SHIELD Tablet K1 | Android.6.0.1 | Chrome Mobile WebView.55.0.2883", "Mozilla/5.0 (Linux; Android 6.0.1; SHIELD Tablet K1 Build/MRA58K; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/55.0.2883.91 Safari/537.36");
    map.put("Samsung SM-T827R4 | Android.7.0 | Chrome.60.0.3112", "Mozilla/5.0 (Linux; Android 7.0; SM-T827R4 Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.116 Safari/537.36");
    map.put("Samsung SM-T550 | Android.5.0.2 | Samsung Internet.3.3", "Mozilla/5.0 (Linux; Android 5.0.2; SAMSUNG SM-T550 Build/LRX22G) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/3.3 Chrome/38.0.2125.102 Safari/537.36");
    map.put("Kindle Fire HDX 7\" WiFi | Android.4.4.3 | Amazon Silk.47.1.79", "Mozilla/5.0 (Linux; Android 4.4.3; KFTHWI Build/KTU84M) AppleWebKit/537.36 (KHTML, like Gecko) Silk/47.1.79 like Chrome/47.0.2526.80 Safari/537.36");
    map.put("LG-V410 | Android.5.0.2 | Chrome Mobile WebView.34.0.1847", "Mozilla/5.0 (Linux; Android 5.0.2; LG-V410/V41020c Build/LRX22G) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/34.0.1847.118 Safari/537.36");

    //PC
    map.put("Other | Windows.10 | Edge.12.246", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.246");
    map.put("Other | Chrome OS.8172.45.0 | Chrome.51.0.2704", "Mozilla/5.0 (X11; CrOS x86_64 8172.45.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.64 Safari/537.36");
    map.put("Other | Mac OS X.10.11.2 | Safari.9.0.2", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/601.3.9 (KHTML, like Gecko) Version/9.0.2 Safari/601.3.9");
    map.put("Other | Windows.7 | Chrome.47.0.2526", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36");
    map.put("Other | Ubuntu | Firefox.15.0.1", "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0) Gecko/20100101 Firefox/15.0.1");

    //bot
    map.put("Spider | Other | Googlebot.2.1", "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)");
    map.put("Spider | Other | bingbot.2.0", "Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)");
    map.put("Spider | Other | Yahoo! Slurp", "Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)");

    map.forEach((except, ua) -> {
      String actual =
          UserAgentUtil.deviceInfo(ua)
              + " | " + UserAgentUtil.osInfo(ua)
              + " | " + UserAgentUtil.browserInfo(ua);
      log.info("isMobile? {}: {}", UserAgentUtil.isMobile(ua), actual);
      assertThat(actual).isEqualTo(except);
    });
  }
}
