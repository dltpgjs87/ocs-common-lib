package com.ogqcorp.ocs.utils;

import java.util.Optional;
import org.apache.commons.lang3.StringUtils;

public class CharacterUtil {
    private CharacterUtil(){}

    public static String toHalf(String input) {
        char[] c = input.toCharArray();
        for (int i = 0; i < c.length; i++) {
            if (c[i] == 12288) {
                //The full-width space is 12288, and the half-width space is 32.
                c[i] = (char) 32;
                continue;
            }
            if (c[i] > 65280 && c[i] < 65375)
                //The correspondence between the other characters half angle (33-126) and the full angle (65281-65374) is: the average difference is 65248
                c[i] = (char) (c[i] - 65248);
        }
        return new String(c);
    }

    public static String toFull(String input)
    {
        //Half angle to full angle:
        char[] c=input.toCharArray();
        for (int i = 0; i < c.length; i++)
        {
            if (c[i]==32)
            {
                c[i]=(char)12288;
                continue;
            }
            if (c[i]<127)
                c[i]=(char)(c[i]+65248);
        }
        return new String(c);
    }

    public static String[] splitStrings(String source, String regex) {
        if(StringUtils.isEmpty(source) || !source.contains(regex)){
            String s = Optional.ofNullable(source).orElseGet(() ->"");
            return new String[]{s};
        }

        return source.split(regex);
    }

    public static boolean isNullOrEmpty(String source) {
        return source == null || source.trim().isEmpty();
    }
}
