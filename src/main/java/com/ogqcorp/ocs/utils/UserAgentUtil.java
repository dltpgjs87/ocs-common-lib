package com.ogqcorp.ocs.utils;

import java.io.IOException;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import ua_parser.Client;
import ua_parser.Device;
import ua_parser.OS;
import ua_parser.Parser;
import ua_parser.UserAgent;

/**
 * Created by OGQ Corp. User: iseheon Created At: 02/10/2019 12:46 오후 Last Modified At: 02/10/2019
 */
@Slf4j
public class UserAgentUtil {
  private UserAgentUtil(){}
  private static final String MOBILE = "mobile";
  private static Parser parser;
  static {
    try {
      parser = new Parser();
      log.debug("Init UserAgentUtil");
    } catch (IOException e) {
      log.error("UserAgent Parser Create Failed: ", e);
    }
  }
  private static synchronized Parser getParser() {
    return parser;
  }

  private static Client parseClient(String ua) {
    return getParser().parse(ua);
  }
  public static boolean isMobile(String ua) {
    Client client = parseClient(ua);
    String userAgentFamily = client.userAgent.family;
    return StringUtils.containsIgnoreCase(userAgentFamily, MOBILE);
  }

  public static String deviceInfo(String ua) {
    Client client = parseClient(ua);
    Optional<Device> optionalDevice = Optional.ofNullable(client.device);
    return optionalDevice.isPresent() ? optionalDevice.get().family : "";
  }
  public static String osInfo(String ua) {
    Client client = parseClient(ua);
    Optional<OS> optionalOS = Optional.ofNullable(client.os);

    return optionalOS.isPresent()
        ? new StringBuilder(optionalOS.get().family)
          .append(".").append(optionalOS.get().major)
          .append(".").append(optionalOS.get().minor)
          .append(".").append(optionalOS.get().patch)
          .toString().replaceAll(".null", "")
        : "";
  }
  public static String browserInfo(String ua) {
    Client client = parseClient(ua);
    Optional<UserAgent> optionalUserAgent = Optional.ofNullable(client.userAgent);

    return optionalUserAgent.isPresent()
          ? new StringBuilder(optionalUserAgent.get().family)
            .append(".").append(optionalUserAgent.get().major)
            .append(".").append(optionalUserAgent.get().minor)
            .append(".").append(optionalUserAgent.get().patch)
            .toString().replaceAll(".null", "")
          : "";
  }
}
