package com.ogqcorp.ocs.utils;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.TimeZone;

public class TimeUtil {
    private static final String SEOUL = "Asia/Seoul";

    public static ZonedDateTime utcnow() {
        return ZonedDateTime.now(ZoneOffset.UTC);
    }

    public static ZonedDateTime toKorTime(ZonedDateTime zonedDateTime) {
        return zonedDateTime.withZoneSameInstant(ZoneId.of("Asia/Seoul"));
    }

    public static TimeZone seoulTimezone() {
        return TimeZone.getTimeZone("Asia/Seoul");
    }

    public static ZonedDateTime krnow() {
        return ZonedDateTime.now(ZoneId.of("Asia/Seoul"));
    }

    public static ZonedDateTime fromEpochSecond(long secs) {
        Instant i = Instant.ofEpochSecond(secs);
        return ZonedDateTime.ofInstant(i, ZoneOffset.UTC);
    }

    public static ZonedDateTime fromKRTimeYMDT(String ymdt) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        ZonedDateTime krTime = ZonedDateTime.parse(ymdt, formatter.withZone(ZoneId.of("Asia/Seoul")));
        return krTime.withZoneSameInstant(ZoneOffset.UTC);
    }

    public static ZonedDateTime fromKRTimeYMDTWidhDash(String ymdt) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        ZonedDateTime krTime = ZonedDateTime.parse(ymdt, formatter.withZone(ZoneId.of("Asia/Seoul")));
        return krTime.withZoneSameInstant(ZoneOffset.UTC);
    }

    public static ZonedDateTime nowDateWithZoneId(ZoneId zoneId) {
        return ZonedDateTime.now(zoneId).truncatedTo(ChronoUnit.DAYS).plusDays(1).withZoneSameInstant(ZoneOffset.UTC);
    }
}
