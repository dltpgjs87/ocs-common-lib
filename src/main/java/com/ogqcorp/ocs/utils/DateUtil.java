package com.ogqcorp.ocs.utils;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.regex.Pattern;

/**
 * Created by OGQ Corp. User: iseheon Created At: 2019/12/12 3:23 오후 Last Modified At: 2019/12/12
 */
public class DateUtil {
  private static final String AGE_EXPR = "^[0-9]{8}$";

  /**
   * 만나이 확인
   * @param birth YYYYMMDD 형식의 문자열
   * @return 만나이 리턴 (YYYYMMDD 형식이 아니라면, -1)
   */
  public static int getAgeFromBirth(String birth) {
    return Optional.ofNullable(birth).map(DateUtil::parseAge).orElse(-1);

  }
  private static final int parseAge(String birth) {
    Pattern agePattern = Pattern.compile(AGE_EXPR);
    if(agePattern.matcher(birth).find()) {
      LocalDate birthDate = LocalDate.parse(birth, DateTimeFormatter.BASIC_ISO_DATE);
      return Period.between(birthDate, LocalDate.now()).getYears();
    }
    return -1;
  }
}
