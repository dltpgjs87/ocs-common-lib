package com.ogqcorp.ocs.filter;


import java.io.IOException;
import java.util.Locale;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.filter.OncePerRequestFilter;

public class HttpMethodOverrideHeaderFilter extends OncePerRequestFilter {
    private static final String X_HTTP_METHOD_OVERRIDE_HEADER = "X-HTTP-Method-Override";

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        String overrideMethod = request.getHeader(X_HTTP_METHOD_OVERRIDE_HEADER);
        if (canOverride(request.getMethod(), overrideMethod)) {
            HttpServletRequest wrapper = new HttpMethodRequestWrapper(request, overrideMethod.toUpperCase(Locale.ENGLISH));
            filterChain.doFilter(wrapper, response);
        }
        else {
            filterChain.doFilter(request, response);
        }
    }

    private boolean canOverride(String reqMethod, String overrideMethod) {
        if(!RequestMethod.POST.name().equals(reqMethod) || StringUtils.isEmpty(overrideMethod)) {
            return false;
        }
        return RequestMethod.DELETE.name().equalsIgnoreCase(overrideMethod) ||
                RequestMethod.PUT.name().equalsIgnoreCase(overrideMethod);
    }

    private static class HttpMethodRequestWrapper extends HttpServletRequestWrapper {
        private final String method;

        public HttpMethodRequestWrapper(HttpServletRequest request, String method) {
            super(request);
            this.method = method;
        }

        @Override
        public String getMethod() {
            return this.method;
        }
    }
}
