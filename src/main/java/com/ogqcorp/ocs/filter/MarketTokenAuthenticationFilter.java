package com.ogqcorp.ocs.filter;

import com.ogqcorp.ocs.auth.InternalTokenService;
import com.ogqcorp.ocs.auth.MarketToken;
import java.io.IOException;
import java.util.List;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

public class MarketTokenAuthenticationFilter extends OncePerRequestFilter {
    private final static String X_MARKET_SCHEME = "X-MARKET-AUTH-TOKEN";

    private InternalTokenService internalTokenService;

    public MarketTokenAuthenticationFilter(InternalTokenService internalTokenService) {
        this.internalTokenService = internalTokenService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String token = request.getHeader(X_MARKET_SCHEME);
        if(!StringUtils.isEmpty(token)) {
            MarketToken marketToken = internalTokenService.fromToken(token);
            List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(marketToken.getRole());
            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(marketToken, null, authorities);
            SecurityContextHolder.getContext().setAuthentication(auth);
        }

        filterChain.doFilter(request, response);
    }
}
