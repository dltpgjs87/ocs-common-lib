package com.ogqcorp.ocs.filter;


import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

public class ResponseBodyXSSFilter extends OncePerRequestFilter {
    private Set<String> excludePatterns = new HashSet<>();
    private AntPathMatcher antPathMatcher = new AntPathMatcher();

    public ResponseBodyXSSFilter(String ...excludes) {
        excludePatterns.addAll(Arrays.asList(excludes));
    }

    private boolean needFiltering(HttpServletRequest request) {
        for(String pattern: excludePatterns) {
            if(antPathMatcher.match(pattern, request.getServletPath())) {
                return false;
            }
        }
        return true;
    }



    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        if(needFiltering(request)) {
            filterChain.doFilter(request, new XSSFilteredResponseWrapper(response));
            return;
        }
        filterChain.doFilter(request, response);

    }

    private static class XSSFilteredServletOutputStream extends ServletOutputStream {
        private ServletOutputStream sos;
        public XSSFilteredServletOutputStream(ServletOutputStream outputStream) {
            sos = outputStream;
        }

        @Override
        public boolean isReady() {
            return true;
        }

        @Override
        public void setWriteListener(WriteListener listener) {
            throw new RuntimeException("Not implemented");
        }

        @Override
        public void write(int b) throws IOException {
            char ch = (char)b;
            switch (ch) {
                case '&':
                    sos.write("&amp;".getBytes());
                    break;
                case '<':
                    sos.write("&lt;".getBytes());
                    break;
                case '>':
                    sos.write("&gt;".getBytes());
                    break;
                case '¢':
                    sos.write("&cent;".getBytes());
                    break;
                case '£':
                    sos.write("&pound;".getBytes());
                    break;
                case '¥':
                    sos.write("&yen".getBytes());
                    break;
                case '§':
                    sos.write("&sect;".getBytes());
                    break;

                case '©':
                    sos.write("&copy;".getBytes());
                    break;

                case '®':
                    sos.write("&reg;".getBytes());
                    break;

                case '™':
                    sos.write("&trade;".getBytes());
                    break;
                default:
                    sos.write(b);
                    break;

            }
        }

        @Override
        public void flush() throws IOException {
            sos.flush();
        }

        @Override
        public void close() throws IOException {
            sos.close();
        }
    }

    private static class XSSFilteredResponseWrapper extends HttpServletResponseWrapper {
        private XSSFilteredServletOutputStream xfos = null;
        private PrintWriter writeWrapper = null;
        /**
         * Constructs a response adaptor wrapping the given response.
         *
         * @param response The response to be wrapped
         * @throws IllegalArgumentException if the response is null
         */
        public XSSFilteredResponseWrapper(HttpServletResponse response) throws IOException {
            super(response);
        }

        @Override
        public PrintWriter getWriter() throws IOException {

            if (writeWrapper == null) {
                writeWrapper = new PrintWriter(new OutputStreamWriter(getOutputStream(), getResponse().getCharacterEncoding()));
            }

            return writeWrapper;
        }

        @Override
        public ServletOutputStream getOutputStream() throws IOException {
            if(xfos == null) {
                xfos = new XSSFilteredServletOutputStream(getResponse().getOutputStream());
            }
            return xfos;
        }

        @Override
        public void flushBuffer() throws IOException {
            if (writeWrapper != null) {
                writeWrapper.flush();
            } else if (xfos != null) {
                xfos.flush();
            }
        }
    }

}
