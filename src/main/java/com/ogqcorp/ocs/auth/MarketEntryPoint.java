package com.ogqcorp.ocs.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ogqcorp.ocs.constants.ResponseCodes;
import com.ogqcorp.ocs.interfaces.res.FailureRes;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;


public class MarketEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        sendUnauthorized(response);
    }

    private void sendUnauthorized(HttpServletResponse res) throws IOException {
        res.setContentType("application/json");
        res.setCharacterEncoding("utf-8");
        res.setHeader("WWW-Authenticate", "Bearer");
        res.setStatus(HttpStatus.UNAUTHORIZED.value());

        ObjectMapper objectMapper = new ObjectMapper();
        String jsonStr = objectMapper.writeValueAsString(new FailureRes(ResponseCodes.UNAUTHORIZED, "required authorized"));
        ServletOutputStream os = res.getOutputStream();
        os.print(jsonStr);
        os.flush();
        os.close();
    }
}
