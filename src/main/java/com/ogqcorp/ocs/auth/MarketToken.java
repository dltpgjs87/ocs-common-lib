package com.ogqcorp.ocs.auth;

import java.time.ZonedDateTime;
import lombok.Data;

@Data
public class MarketToken {
    private String transId;
    private String marketId;
    private String userId;
    private String username;
    private String nickName;
    private String userType;
    private String profileUrl;
    private String status;
    private String role;
    private String marketUserId;
    private String extra;
    private ZonedDateTime createdAt;

    public MarketToken(String transId, String marketId, String userId, String username, String nickName, String userType,
                       String profileUrl, String status, String role, String marketUserId, String extra, ZonedDateTime createdAt) {
        this.transId = transId;
        this.marketId = marketId;
        this.userId = userId;
        this.username = username;
        this.nickName = nickName;
        this.userType = userType;
        this.profileUrl = profileUrl;
        this.status = status;
        this.role = role;
        this.marketUserId = marketUserId;
        this.extra = extra;
        this.createdAt = createdAt;
    }

}
