package com.ogqcorp.ocs.constants;

public enum SalePolicyCode {
    STREAM {
        @Override
        public boolean isDownloadable() {
            return false;
        }
    },
    DOWNLOAD,
    AUDIO_BASIC,
    AUDIO_STANDARD,
    AUDIO_EXTENDED;

    public boolean isDownloadable() {
        return true;
    }
}