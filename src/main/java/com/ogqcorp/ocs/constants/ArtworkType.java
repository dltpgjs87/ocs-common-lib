package com.ogqcorp.ocs.constants;

public enum ArtworkType {
    STICKER, COLORING_SHEET, AUDIO, STOCK_IMAGE
}
