package com.ogqcorp.ocs.constants;

/**
 * Created by OGQ Corp. User: iseheon Created At: 30/10/2019 3:17 오후 Last Modified At: 30/10/2019
 */
public class HttpHeader {
  public static final String TOKEN = "token";
  public static final String X_FORWARDED_FOR = "X-FORWARDED-FOR";    // IP
  public static final String UNIQUE_DEVICE_ID = "uniqueDeviceId";    // Mobile Device Key
  public static final String USER_AGENT = "User-Agent";              // User Agent

  public static final String X_MARKET_ID = "X-MARKET-ID";           // MarketID

}
