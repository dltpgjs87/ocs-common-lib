package com.ogqcorp.ocs.constants;

/**
 * Created by Joohan Lee on 2020/02/19
 */
public enum ArtworkStatus {
  INIT,
  REGISTERED,
  REQUEST,
  REVIEW ,
  CONFIRM ,
  CANCEL,
  WAIT_SALE,
  SALE,
  SALE_STOP,
  DELETED
}
