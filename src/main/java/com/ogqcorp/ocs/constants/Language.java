package com.ogqcorp.ocs.constants;

public enum Language {
    EN, KO;

    public String lower() {
        return this.name().toLowerCase();
    }
}
