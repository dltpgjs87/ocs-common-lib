package com.ogqcorp.ocs.constants;

public class UserRole {
    public static final String ADMIN = "ADMIN";
    public static final String OPERATOR = "OPERATOR";

    public static final String JOINING_CREATOR = "JOINING_CREATOR";
    public static final String NO_AUTH_CREATOR = "NO_AUTH_CREATOR";
    public static final String NOT_AGREE_CREATOR = "NOT_AGREE_CREATOR";
    public static final String CREATOR = "CREATOR";

    public static final String CUSTOMER = "CUSTOMER";
    public static final String NOT_AGREE_CUSTOMER = "NOT_AGREE_CUSTOMER";
    public static final String NAVER_USER = "NAVER_USER";
    public static final String NOT_AGREE_NAVER_USER = "NOT_AGREE_NAVER_USER";

    public static final String PARTNER = "PARTNER";
    public static final String ROLE = "ROLE_";

    public static final String ROLE_ADMIN = ROLE + ADMIN;
    public static final String ROLE_OPERATOR = ROLE + OPERATOR;

    public static final String ROLE_NO_AUTH_CREATOR = ROLE + NO_AUTH_CREATOR;
    public static final String ROLE_NOT_AGREE_CREATOR = ROLE + NOT_AGREE_CREATOR;
    public static final String ROLE_CREATOR = ROLE + CREATOR;


    public static final String ROLE_CUSTOMER = ROLE + CUSTOMER;
    public static final String ROLE_NOT_AGREE_CUSTOMER = ROLE + NOT_AGREE_CUSTOMER;
    public static final String ROLE_NAVER_USER = ROLE + NAVER_USER;

    public static final String ROLE_PARTNER = ROLE + PARTNER;

    public static final String ROLE_NOT_AGREE_NAVER_USER = ROLE + NOT_AGREE_NAVER_USER;
}
