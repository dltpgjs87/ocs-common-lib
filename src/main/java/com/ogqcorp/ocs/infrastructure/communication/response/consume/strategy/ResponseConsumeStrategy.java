package com.ogqcorp.ocs.infrastructure.communication.response.consume.strategy;


import com.ogqcorp.ocs.infrastructure.communication.response.IResponse;
import com.ogqcorp.ocs.infrastructure.communication.response.consume.delegator.IResponseConsumeDelegator;
import java.util.function.Consumer;

/**
 * Created by OGQ Corp. User: iseheon Created At: 2020/02/19 12:12 오후 Last Modified At: 2020/02/19
 */
public abstract class ResponseConsumeStrategy implements Consumer<IResponse> {

  private IResponseConsumeDelegator<IResponse> iResponseConsumeDelegator;

  public ResponseConsumeStrategy(IResponseConsumeDelegator<IResponse> iResponseConsumeDelegator) {
    this.iResponseConsumeDelegator = iResponseConsumeDelegator;
  }

  @Override
  public final void accept(IResponse iResponse) {
    if(iResponse.isSuccess()) {
      succeed(iResponse);
    } else {
      failed(iResponse);
    }
  }

  protected void succeed(IResponse r) {
    iResponseConsumeDelegator.consume(r);
  }
  protected void failed(IResponse r) {
    iResponseConsumeDelegator.consume(r);
  }
}
