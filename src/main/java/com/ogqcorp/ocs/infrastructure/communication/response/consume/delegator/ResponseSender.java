package com.ogqcorp.ocs.infrastructure.communication.response.consume.delegator;

/**
 * Created by OGQ Corp. User: iseheon Created At: 2020/02/20 4:36 오후 Last Modified At: 2020/02/20
 */
public abstract class ResponseSender<R> implements IResponseConsumeDelegator<R> {

  @Override
  public final void consume(R r) {
    send(r);
  }

  protected abstract void send(R r);
}
