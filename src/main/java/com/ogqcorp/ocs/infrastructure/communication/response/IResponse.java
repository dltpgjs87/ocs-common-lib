package com.ogqcorp.ocs.infrastructure.communication.response;

/**
 * Created by OGQ Corp. User: iseheon Created At: 2020/02/19 12:08 오후 Last Modified At: 2020/02/19
 */
public interface IResponse {
  boolean isSuccess();
}
