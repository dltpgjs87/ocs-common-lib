package com.ogqcorp.ocs.infrastructure.communication.response.consume.delegator;

/**
 * Created by OGQ Corp. User: iseheon Created At: 2020/02/19 12:27 오후 Last Modified At: 2020/02/19
 */
public abstract class ResponseWriter<R> implements IResponseConsumeDelegator<R> {

  @Override
  public final void consume(R r) {
    write(r);
  }

  protected abstract void write(R r);
}
