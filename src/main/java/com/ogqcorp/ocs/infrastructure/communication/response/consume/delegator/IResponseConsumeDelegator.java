package com.ogqcorp.ocs.infrastructure.communication.response.consume.delegator;

/**
 * Created by OGQ Corp. User: iseheon Created At: 2020/02/20 7:14 오후 Last Modified At: 2020/02/20
 */
public interface IResponseConsumeDelegator<R> {
  void consume(R r);
}
