package com.ogqcorp.ocs.infrastructure.communication.response.consume.strategy;

import com.ogqcorp.ocs.infrastructure.communication.response.consume.delegator.ResponseSender;

/**
 * Created by OGQ Corp. User: iseheon Created At: 2020/02/19 7:17 오후 Last Modified At: 2020/02/19
 */
public class ResponseSendStrategy extends ResponseConsumeStrategy {

  public ResponseSendStrategy(ResponseSender responseSender) {
    super(responseSender);
  }

}
