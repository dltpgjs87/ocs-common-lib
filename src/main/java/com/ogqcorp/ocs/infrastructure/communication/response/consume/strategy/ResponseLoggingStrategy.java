package com.ogqcorp.ocs.infrastructure.communication.response.consume.strategy;

import com.ogqcorp.ocs.infrastructure.communication.response.consume.delegator.ResponseWriter;

/**
 * Created by OGQ Corp. User: iseheon Created At: 2020/02/19 3:03 오후 Last Modified At: 2020/02/19
 */
public class ResponseLoggingStrategy extends ResponseConsumeStrategy {

  public ResponseLoggingStrategy(ResponseWriter iResponseWriter) {
    super(iResponseWriter);
  }

}
