package com.ogqcorp.ocs.interfaces.res;

import io.swagger.annotations.ApiModelProperty;

public class BaseResponse {
    @ApiModelProperty(required = true)
    int code;

    public BaseResponse(int code) {
        this.code = code;
    }

    public BaseResponse() {}

    public int getCode() {
        return code;
    }
}
