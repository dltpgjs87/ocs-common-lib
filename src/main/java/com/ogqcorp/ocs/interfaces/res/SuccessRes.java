package com.ogqcorp.ocs.interfaces.res;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ogqcorp.ocs.constants.ResponseCodes;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode(callSuper = false)
public class SuccessRes<T> extends BaseResponse {
    @ApiModelProperty(required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T data;

    public SuccessRes() {
        super(ResponseCodes.OK);
        data = null;
    }

    public SuccessRes(T data) {
        super(ResponseCodes.OK);
        this.data = data;
    }
}
