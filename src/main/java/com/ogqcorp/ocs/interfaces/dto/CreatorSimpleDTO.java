package com.ogqcorp.ocs.interfaces.dto;

import lombok.Data;

@Data
public class CreatorSimpleDTO {
    private String userId;
    private String name;
    private String creatorLevel;
    private String imageUrl;
    private String introduction;
    private String nickname;
}
