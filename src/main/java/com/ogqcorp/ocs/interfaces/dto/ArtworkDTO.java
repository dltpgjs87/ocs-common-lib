package com.ogqcorp.ocs.interfaces.dto;

import com.ogqcorp.ocs.constants.ArtworkType;
import com.ogqcorp.ocs.constants.Language;
import com.ogqcorp.ocs.constants.SalePolicyCode;
import java.util.List;
import java.util.Set;
import lombok.Data;

@Data
public class ArtworkDTO {
    public int getPrice(SalePolicyCode policyCode) {
        if(!salesPolicies.isEmpty()) {
            for (SalesPolicyDTO salesPolicy : salesPolicies) {
                if (salesPolicy.getPolicyCode() == policyCode) {
                    return salesPolicy.getAmount();
                }
            }
        }
        return -1;
    }

    @Data
    public static class SalesPolicyDTO {
        private SalePolicyCode policyCode;
        private int amount;
        private String currency;

    }

    @Data
    public static class TextContentDTO {
        private Language lang;
        private String title;
        private String description;
    }

    private String artworkId;
    private String mainImageUrl;

    private List<TextContentDTO> textContents;
    private Set<String> tags;

    private ArtworkType type;
    private String status;

    private List<SalesPolicyDTO> salesPolicies;
    private long createdAt;

    private CreatorSimpleDTO creator;

    private boolean resourceUpdatable;
    private boolean isDownloadable;
    private String defaultName;

    public boolean isSales() {
        return status.equals("SALE");
    }
}
