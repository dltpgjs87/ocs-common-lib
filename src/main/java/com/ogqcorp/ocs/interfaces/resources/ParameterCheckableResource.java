package com.ogqcorp.ocs.interfaces.resources;

import com.ogqcorp.ocs.constants.ResponseCodes;
import com.ogqcorp.ocs.interfaces.res.FailureRes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;

public class ParameterCheckableResource {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity handleClientParameterException(MethodArgumentNotValidException ex) {
        ObjectError error = ex.getBindingResult().getAllErrors().get(0);
        String message = "Wrong parameters";
        if(error instanceof FieldError) {
            FieldError fieldError = (FieldError)error;
            message = fieldError.getField() + " " + fieldError.getDefaultMessage();
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new FailureRes(ResponseCodes.BAD_REQUEST,
                message));
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity handleParameterConvertException(HttpMessageNotReadableException ex ) {

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new FailureRes(ResponseCodes.BAD_REQUEST,
                ex.getLocalizedMessage()));
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity handleMissingServletRequestParameterException(MissingServletRequestParameterException ex ) {

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new FailureRes(ResponseCodes.BAD_REQUEST,
                ex.getLocalizedMessage()));
    }
}
