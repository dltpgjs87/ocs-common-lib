package com.ogqcorp.ocs.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ogqcorp.ocs.constants.UserRole;
import com.ogqcorp.ocs.interfaces.res.FailureRes;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;

public class MarketAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest req, HttpServletResponse res, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        res.setContentType("application/json");
        res.setCharacterEncoding("utf-8");
        res.setStatus(HttpStatus.FORBIDDEN.value());

        SecurityContextHolderAwareRequestWrapper requestWrapper = (SecurityContextHolderAwareRequestWrapper) req;

        ObjectMapper objectMapper = new ObjectMapper();
        String jsonStr;

        if(requestWrapper.isUserInRole(UserRole.NO_AUTH_CREATOR)) {
            res.setStatus(HttpStatus.PROXY_AUTHENTICATION_REQUIRED.value());
            jsonStr = objectMapper.writeValueAsString(new FailureRes(
                    40304, "need email verification."));
        } else if(requestWrapper.isUserInRole(UserRole.NOT_AGREE_CREATOR)) {
            res.setStatus(HttpStatus.PROXY_AUTHENTICATION_REQUIRED.value());
            jsonStr = objectMapper.writeValueAsString(new FailureRes(
                    40302, "need to agree terms"));
        } else {
            jsonStr = objectMapper.writeValueAsString(new FailureRes(
                    40300, "unAuthorized"));
        }

        ServletOutputStream os = res.getOutputStream();
        os.print(jsonStr);
        os.flush();
        os.close();
    }
}
